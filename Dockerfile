FROM gradle:jdk8
RUN wget https://repo1.maven.org/maven2/io/prometheus/jmx/jmx_prometheus_javaagent/0.14.0/jmx_prometheus_javaagent-0.14.0.jar
COPY source/build/libs/* ./
RUN printf "username:\npassword:" > config.yaml
ENTRYPOINT java -javaagent:./jmx_prometheus_javaagent-0.14.0.jar=5678:config.yaml -jar tchallenge-service.jar | tee /var/log/tbackend.log
